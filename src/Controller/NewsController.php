<?php

namespace App\Controller;

use App\Repository\NewsRepository;
use App\Services\NewsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{

    /**
     * @var NewsService
     */
    private $newsService;

    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }


    /**
     * @param $id
     * @Route("/news/{id}", name="getNewы", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getNews($id)
    {
        $news = $this->newsService->getNews($id);

        return $this->json([
            'message' => 'GET',
            'data' => $news,
            'path' => 'src/Controller/NewsController.php',
        ]);
    }

    /**
     * @param Request $request
     * @Route("/news", name="addNews", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     */
    public function postNews(Request $request)
    {
        #TODO Validation

        $news = $this->newsService->addNews(
            $request->title,
            $request->text
        );
        return $this->json([
            'message' => 'POST',
            'path' => 'src/Controller/NewsController.php',
        ]);
    }


    /**
     * @Route("/news/{id}", name="updateNews", methods={"PUT"})
     */
    public function putNews($id, Request $request)
    {
        return $this->json([
            'method' => 'PUT',
            'id' => $id,
            'path' => 'src/Controller/NewsController.php',
        ]);
    }


    /**
     * @Route("/news/{id}", name="updateNews", methods={"DELETE"})
     */
    public function deleteNews($id, Request $request)
    {
        return $this->json([
            'method' => 'DELETE',
            'id' => $id,
            'path' => 'src/Controller/NewsController.php',
        ]);
    }
}
