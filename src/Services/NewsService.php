<?php

namespace App\Services;

use \App\Entity\News;
use App\Repository\NewsRepository;


class NewsService
{
    /**
     * @var NewsRepository
     */
    private $newsRepository;

    public function __construct(NewsRepository $postRepository)
    {
        $this->newsRepository = $postRepository;
    }

    public function getNews(int $id)
    {
        return $this->newsRepository->findById($id);

    }

    public function addNews(string $title, string $text)
    {
        $news = new News();
        $news->setTitle($title);
        $news->setText($text);
        $this->newsRepository->save($news);

        return $news;
    }


    public function updateNews()
    {
        //$post =
    }

}